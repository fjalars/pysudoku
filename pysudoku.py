#!/usr/bin/python
# -*- coding: utf-8 -*-

# ------------------------------- #
#
# pySudoku 0.1 Alpha
# Code made by Fjalar Sigurdarson / fjalars@gmail.com   
# August 2017
#
# ------------------------------- #

'''
pysudoku.py 0.1 -- Sudoku analyser, solver and generator

Created by Fjalar Sigurdarson.
2017
'''


# general modules
import argparse, os, sys, signal, time

class Sudoku(object):
   ''' Soduko class for creating and solving sudoku games'''

   def __init__(self):
      #print '>> Sudoku class instantiated'

      # Game board   
      self.GAME = []
      self.GAME_SOLVED = False

      # Column / Row analysis
      self.CR_ANALYSIS = []
      self.CR_ANALYSIS_SIZE = []

      # Box analysis
      self.BOX_ANALYSIS = []
      self.BOX_ANALYSIS_SIZE = []

      self.NUMBER_OF_CLUES = 0

      self.HISTOGRAM = {'0':0, '1':0, '2':0, '3':0, '4':0, '5':0, '6':0, '7':0, '8':0, '9':0}
  
   def load(self,game_string):
      '''Load a sting containing a game to be stored in a game structure'''
      print '>> Loading game string to structure'

      for string in game_string:

         # define a row array
         row = []

         # trim new line character away
         string = string.rstrip('\n')

         # exchange '.' for None and add values to a array
         for value in string:
            if value == '.':
               row.append(None)
            else:
               row.append(int(value))

         self.GAME.append(row)

      # DEBUG #
      #for i in self.GAME:
      #   print i
   
   def create(self,level):
      '''Method that creates a game, given a diffulty level, and returns a game object'''
      print '>> create_game() called.'
      print '>> difficulty level: {}'.format(level)
   
   def solve_simple(self):
      '''Method that receives a game object and solves it'''
      print '>> solve_game() called.'
      print '>> Method not yet implemented.'
    
   def solve_spaces(self):
      '''Method that receives a game object and solves it'''
      print '>> solve_spaces() called.'

      # Start a "timer"
      start_time = time.time()
     
      # Check game integrity for the first time
      integrity_is_ok = self.analyse_game_integrity()

      if not integrity_is_ok:
         print 'Initial integrity of the game is not ok. Exiting...'
         sys.exit(1)

      two_solutions = []
      backtrack = False

      # Run the solution loop - no stop for hitchhikers
      while not self.GAME_SOLVED and integrity_is_ok:
      
         # Check first for single solution spaces
         # insert these solutions and re-analyse until there are no more.
         while self.HISTOGRAM['1'] > 0:

            for y in range(9):
               for x in range(9):
                  if self.CR_ANALYSIS_SIZE[y][x] == 1:
                     print '>> single solution space found. Inserting solution to the game.'
                     self.GAME[y][x] = self.CR_ANALYSIS[y][x][0]

            # check integrity 
            if not self.analyse_game_integrity():
               # backtrack to the latest two-spaces solution
               backtrack = True
               break
            else:
               self.analyse_solution_spaces()

         # Then work trial-and-error on the two-solution spaces
         if self.HISTOGRAM['2'] > 0 and not backtrack:

            # Array for all current two-solution spaces
            two_solution_spaces = [] 
   
            # Find all current two-solution spaces (not necessary but could be used)
            for y in range(9):
               for x in range(9):
                  if self.CR_ANALYSIS_SIZE[y][x] == 2:
                     print '>> found a double solution space. Storing...'
                     two_solution_spaces.append((y,x))

            # Use the first two-solution space - actually any of the spaces could be picked at random
            # but here we use the first one
            v,u = two_solution_spaces[0]

            # save the game's current state and solution used in an array for backtracking
            game_current_state = self.GAME   
            
            # get the possible solutions and store
            solution_1 = self.CR_ANALYSIS[v][u][0]
            solution_2 = self.CR_ANALYSIS[v][u][1]
            
            # save game status, coordinates and the two solutions
            two_solutions.append({'game':game_current_state, 'v':v, 'u':u, 'value':solution_1})
            two_solutions.append({'game':game_current_state, 'v':v, 'u':u, 'value':solution_2})

            # Set the trial value
            self.GAME[v][u] = two_solutions[0]['value']
            print 'Setting {0} to location {1},{2}'.format(two_solutions[0]['value'],v,u)

            # check integrity 
            if not self.analyse_game_integrity():
               # backtrack to the latest two-spaces solution
               backtrack = True
               break
            else:
               self.analyse_solution_spaces()

         if backtrack:
            two_solutions.pop(0)

         self.print_2_console()
         self.analyse_solution_spaces()
         end_time = time.time() 

      if self.GAME_SOLVED:
         print '>> Game has been solved with solution Spaces method.'
         print 'Time: {0:.4g} seconds'.format(end_time-start_time)


   def print_2_console(self,matrix=None, message=None):
      '''Print a moderately fancy version of a gameboard to the console'''

      if message:
         print ''
         print '{}'.format(message)

      if not matrix:
         matrix = self.GAME
      elif matrix == 'CR_ANALYSIS':
         matrix = self.CR_ANALYSIS
      elif matrix == 'CR_ANALYSIS_SIZE':
         matrix = self.CR_ANALYSIS_SIZE

      print '-------------------------------------'
      for game_row in matrix:
         for game_cell in game_row:
            if not game_cell:
               game_cell = ' '
            print '| {}'.format(game_cell),
         
         print '|'
         print '-------------------------------------'

   def game_2_box_coordinates(self,y,x):

      #print y,x
      # find BOX_ANALYSIS coordinates from game board coordinates -> locate correct sub-box
      # first row
      if y <= 2 and x <= 2: 
         u = 0
         v = 0
      
      if y <= 2 and x > 2 and x <= 5:
         u = 1
         v = 0

      if y <= 2 and x > 5:
         u = 2
         v = 0

      # second row
      if y > 2 and y <= 5 and x <= 2: 
         u = 0
         v = 1
      
      if y > 2 and y <= 5 and x > 2 and x <= 6:
         u = 1
         v = 1

      if y > 2 and y <= 5 and x > 5:
         u = 2
         v = 1

      # third row
      if y > 5 and x <= 2: 
         u = 0
         v = 2
      
      if y > 5 and x > 2 and x <= 5:
         u = 1
         v = 2

      if y > 5 and x > 5:
         u = 2
         v = 2

      #print u,v
      return u,v

   def analyse_game_integrity(self):

      print 'Analysing game integrity'

      # The game board will pass the check until proven otherwise :)
      passed_check = True

      # Scan the rows
      for y in range(9):
         row_numbers = [1,2,3,4,5,6,7,8,9]
         for x in range(9):
            if self.GAME[y][x]:
               try:
                  row_numbers.remove(self.GAME[y][x])
               except Exception as e:
                  #raise e
                  print '>> Row integrity NOT OK: There are two instances of the number {0} in row {1}'.format(self.GAME[y][x],y+1)
                  passed_check = False
                  
      # Scan the columns
      for x in range(9):
         column_numbers = [1,2,3,4,5,6,7,8,9]
         for y in range(9):
            if self.GAME[y][x]:
               try:
                  column_numbers.remove(self.GAME[y][x])
               except:
                  print '>> Column integrity NOT OK: There are two instances of the number {0} in column {1}'.format(self.GAME[y][x],x+1)
                  passed_check = False

      # Scan boxes
      for y in xrange(0,9,3):
         for x in xrange(0,9,3):
            box = [1,2,3,4,5,6,7,8,9]
            for j in range(3):
               for i in range(3):
                  if not self.GAME[y+j][x+i]:
                     pass
                  else:
                     try:
                        box.remove(self.GAME[y+j][x+i])
                     except:
                        print '>> Box integrity NOT OK: There are two instances of the number {0} in the sub-box at {1},{2}'.format(self.GAME[y][x],y+1,x+1)
                        passed_check = False              
                        

      return passed_check

   def analyse_solution_spaces(self):

      # Reset analysis variables 
      # Column / Row analysis
      self.CR_ANALYSIS = []
      self.CR_ANALYSIS_SIZE = []

      # Box analysis
      self.BOX_ANALYSIS = []
      self.BOX_ANALYSIS_SIZE = []

      self.NUMBER_OF_CLUES = 0

      # create an empty analysis matrix
      for y in range(9):
         row = []
         for x in range(9):
            row.append(None)
         self.CR_ANALYSIS_SIZE.append(row)
      
      for y in range(9):
         row = []
         for x in range(9):
            row.append(None)
         self.CR_ANALYSIS.append(row)

      # create empty histogram dict
      histogram = {'0':0, '1':0, '2':0, '3':0, '4':0, '5':0, '6':0, '7':0, '8':0, '9':0}

      # box analysis - sub box solution spaces
      for y in xrange(0,9,3):
         row =[]
         row_size = []
         for x in xrange(0,9,3):
            box_solution_space = [1,2,3,4,5,6,7,8,9]
            for j in range(3):
               for i in range(3):
                  if not self.GAME[y+j][x+i]:
                     pass
                  else:
                     try:
                        box_solution_space.remove(self.GAME[y+j][x+i])
                     except:
                        pass 
            
            row.append(box_solution_space)
            row_size.append(len(box_solution_space))
         
         self.BOX_ANALYSIS.append(row)
         self.BOX_ANALYSIS_SIZE.append(row_size)
            
      
      ## FIX: Move to print_2_console ------- ##
      print ''
      print 'Sub-box solution spaces'
      for row in self.BOX_ANALYSIS:
         print row

      print ''
      print 'Sub-box solution spaces size'
      for row in self.BOX_ANALYSIS_SIZE:
         print row
      ## FIX --------------------------------- ##

      # row-column analysis
      for y in range(9):
         for x in range(9):

            # count clues in the game
            if self.GAME[y][x]:
               self.NUMBER_OF_CLUES = self.NUMBER_OF_CLUES +1

            # find the empty cells and calculate solution spaces           
            if not self.GAME[y][x]:

               # initiate solution space
               solution_space = [1,2,3,4,5,6,7,8,9]
 
               for i in range(9):
                  # run through the x-axis / row, skipping over empty cells 
                  if not self.GAME[y][i]:
                     pass
                  else:
                     try:
                        #print '>> Removing {0} found in "X" solution space'.format(self.GAME[y][i])
                        solution_space.remove(self.GAME[y][i])
                     except:
                        pass
                     
                  # run through the y-axis / column, skipping over empty cells 
                  if not self.GAME[i][x]:
                     pass
                  else:
                     try:
                        #print '>> Removing {0} found in "Y" solution space'.format(self.GAME[i][x])
                        solution_space.remove(self.GAME[i][x])
                     except:
                        pass

                  v,u = self.game_2_box_coordinates(y,x)

                  # cross reference and correct for solutions that do not exist in the relevant 
                  # sub-box solution space
                  for solution in solution_space:
                     if not solution in self.BOX_ANALYSIS[u][v]:
                        solution_space.remove(solution)

               self.CR_ANALYSIS[y][x] = solution_space
               self.CR_ANALYSIS_SIZE[y][x] = len(solution_space)

               histogram[str(len(solution_space))] = histogram[str(len(solution_space))] + 1


      # print results from the solution space analysis
      self.print_2_console('CR_ANALYSIS_SIZE','Solution space size for each cell')
      self.print_2_console('CR_ANALYSIS','Solution spaces for each cell')
      
      # Statistics
      print ''
      print 'Histogram for solution spaces'
      sum_of_values = 0
      number_of_values = 0
      mode = 0

      for key in range(9):
         # histogram
         value = histogram[str(key+1)]
         occur = '*' * value
         print '{0}: {1} ({2})'.format(key+1,occur,value)

         # useful calculations
         sum_of_values = sum_of_values + int(key+1)*int(value)
         number_of_values = number_of_values + int(value)

      # check if game has been / is already solved
      if number_of_values == 0:
         self.GAME_SOLVED = True

      else:
         print ''
         print 'Other statistics'
         print 'Sum of values: {}'.format(sum_of_values)
         print 'Number of values: {}'.format(number_of_values)
         print 'Average: {0}'.format(float(sum_of_values)/float(number_of_values))
         print 'Number of clues: {0}'.format(self.NUMBER_OF_CLUES)
            
      self.HISTOGRAM = histogram

## Main methods ##

def validate_args(args):
   ''' Validate input arguments before going further.'''
   if not args.file and not args.create_game:
      print 'File argument missing. Please use -h / --help for help.'

def exit_gracefully(signum, frame):
   ''' Exit gracefully on Ctrl-C '''

   # restore the original signal handler as otherwise evil things will happen
   # in raw_input when CTRL+C is pressed, and our signal handler is not re-entrant
   signal.signal(signal.SIGINT, original_sigint)

   try:
      if raw_input("\nReally quit? (y/n)> ").lower().startswith('y'):
         sys.exit(1)

   except KeyboardInterrupt:
      print 'Ok ok, quitting'
      sys.exit(1)

   # restore the exit gracefully handler here
   signal.signal(signal.SIGINT, exit_gracefully)

   # Method borrowed from:
   # http://stackoverflow.com/questions/18114560/python-catch-ctrl-c-command-prompt-really-want-to-quit-y-n-resume-executi

def program_info_screen():
   ''' Print software info'''

   print ''
   print '*-----------------------------------------------------*'
   print 'pySudoku 0.1 -- Sudoku analyser, solver and generator'
   print 'August 2017'
   print 'Created by Fjalar Sigurdarson (fjalars@gmail.com)'
   print '*-----------------------------------------------------*'


def main():
   ''' main '''

   # Display some nice program info
   program_info_screen()

   # Instantiate argparser
   parser = argparse.ArgumentParser()

   # Setup the argument parser settings
   parser.add_argument('-f', '--file',
                        type=str,
                        help='''Input file containing sudoku game boards to be solved.''')
   parser.add_argument('-a', '--analyse',
                        action='store_true',
                        help='''Run analysis on the game, including solution space analysis.''')
   parser.add_argument('-b', '--solve_simple',
                        action='store_true',
                        help='''Solve a game using a simple brute force backtracking method.''')
   parser.add_argument('-c', '--solve_spaces',
                        action='store_true',
                        help='''Solve a game using an optimized brute force backtrack method using 
                                information regarding solution spaces.''')
   parser.add_argument('-g', '--create_game',
                        action='store_true',
                        help='''Generate a sudoku game. Default level is EASY.''')
   parser.add_argument('-l', '--level',
                        type=str,
                        help='''Set the difficulty level for a generated game. Levels are 
                              EASY / MEDIUM / HARD / SAMURAI.''')
   parser.add_argument('-v', '--version',
                        action='store_true',
                        help='''Version information. ''')

   # Fetch the arguments
   args = parser.parse_args()

   # Validate argumentss
   validate_args(args)

   # Instantiate a new sudoku game object
   sudoku_game = Sudoku()

   if args.file:

      #try:
         with open(args.file) as f:
            content = f.readlines()
         #print content
         sudoku_game.load(content)
         print '>> Printing newly loaded gameboard to console'
         sudoku_game.print_2_console()

         if args.analyse:
            print '>> Analysing game properties.'
            sudoku_game.analyse_solution_spaces()

         if args.solve_simple:
            print 'Game will be solved by a simple method.'
            sudoku_game.solve_simple()

         if args.solve_spaces:
            print 'Game will be soloved by an optimized metod.'
            sudoku_game.solve_spaces()

      #except Exception as e:
      #   raise e
      #   print "Problem with reading game file."

   if args.create_game:
      if args.level:
         if args.level == 'easy' or args.level == 'medium' or args.level == 'hard' or args.level == 'samurai':
            sudoku_game.create(args.level)
         else:
            print 'Invalid option for difficulty level: {}'.format(args.level)
   

if __name__ == '__main__':
   # This is used to catch Ctrl-C exits
   original_sigint = signal.getsignal(signal.SIGINT)
   signal.signal(signal.SIGINT, exit_gracefully)

   main()
